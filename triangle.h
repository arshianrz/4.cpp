#ifndef TRIANGLE_H
#define TRIANGLE_H


class Triangle
{
private:
    int n_,temp_;
public:
    Triangle();
    void setN(int);
    int getN();
    void setTemp(int);
    void display();
    void run();

};

#endif // TRIANGLE_H
