#include "triangle.h"
#include <iostream>

using namespace std;

Triangle::Triangle()
{

 setTemp(1);
}
//***************************************************************************************************************
void Triangle::setN(int N)
{
    n_=N;
}
//***************************************************************************************************************
int Triangle::getN()
{
    return n_;
}
//***************************************************************************************************************
void Triangle::setTemp(int TEMP)
{
    temp_=TEMP;
}
//***************************************************************************************************************
void Triangle::display()
{
    for(int i=n_ ; i>=1 ; i--)
    {
        if(i==1)
        {
            for(int l=1 ; l<=(2*n_) ; l++)
            {
                if (l%2==0)
                    cout<<" ";
                else
                    cout<<"*";
            }
            break;
        }


        for(int k=i-1 ; k>=1 ; k--)
        {
            cout<<" ";
        }

        cout<<"*";

        for(int k=2 ; k<=temp_-1 ; k++ )
        {
            cout<<" ";
        }
        temp_+=2;

        if(i==n_)
        {
            cout<<" ";
        }
        else
        {
            cout<<"*";
        }
        cout<<endl;
    }
    cout<<endl;
}
//***************************************************************************************************************
void Triangle::run()
{
    int a;
    cout<<"Enter Dimension of your triangle. \n";
    cin>>a;
    setN(a);
    getN();
    display();
}
